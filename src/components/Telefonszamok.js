import React, { Component } from 'react';
import { addPhoneNumber, populatePhoneNumbers, removePhoneNumber, filterPhoneNumbers, 
  editPhoneNumber, fetchCustomers, fetchPhoneNumbers } from '../store/actions';
import PropTypes from 'prop-types';
import TelefonszamEditorModal from './TelefonszamEditorModal';

const $ = window.$;

export default class Telefonszamok extends Component {

  constructor(props, context) {
    super(props, context);
    this.http = props.http;
    this.store = context.store;
    this.getTelefonszamok();
    this.highestId = -1;
    this.state = {
      selectedNumber: null
    }
  }

  getTelefonszamok() {
    Promise.all([this.store.dispatch(fetchCustomers(this.http)), 
                this.store.dispatch(fetchPhoneNumbers(this.http))])
      .then(results => {
        const phoneNumbers = results[1];
        const customers = results[0];
        phoneNumbers.forEach(telefonszam => {
          if (this.highestId < telefonszam.id) {
            this.highestId = telefonszam.id;
          }
          telefonszam.ugyfel = customers.find(ugyfel => ugyfel.id === telefonszam.ugyfelId);
        });
        this.store.dispatch(populatePhoneNumbers(phoneNumbers));
      })
      .catch((ex) => {
        console.log('Getting phone numbers failed!', ex)
      });
  }

  addNewTelefonszam() {
    const telefonszam = {
      id: ++this.highestId,
      telefonszam: '',
      ugyfelId: null,
      type: 'new'
    };
    this.switchEditor(null, telefonszam);
  }

  switchEditor(e, telefonszam) {
    this.setState({ selectedNumber: { ...telefonszam } }, () => {
      this.$modal = $(this.refs._modal);
      this.$modal.modal({ backdrop: 'static', keyboard: false });
    });
  }

  closeEditor() {
    this.$modal.modal('hide');
    this.setState({ selectedNumber: null });
  }

  saveChanges(telefonszam) {
    const { type, ugyfel, ...teloszam } = telefonszam;
    this.http.fetch(type === 'new' ? "POST" : "PUT", '/telefonszamok', [teloszam]).then(() => {
      this.updateState(telefonszam);
    });
  }

  updateState(telefonszam) {
    const { type, ...teloszam } = telefonszam;
    if (type === 'new') {
      this.store.dispatch(addPhoneNumber(teloszam));
    } else {
      this.store.dispatch(editPhoneNumber(teloszam));
    }
    this.$modal.modal('hide');
    this.setState({ selectedNumber: null });
  }

  deleteTelefonszam(telefonszam) {
    const { type, ugyfel, ...teloszam } = telefonszam;
    this.http.fetch('DELETE', '/telefonszamok', [teloszam]).then(() => {
      this.store.dispatch(removePhoneNumber(telefonszam));
    });
  }

  filterTelefonszamok(searchText) {
    this.store.dispatch(filterPhoneNumbers(searchText));
  }

  renderTelefonszamok() {
    const state = this.store.getState();
    const filter = state.phoneFilterText;
    return state.phoneNumbers
      .filter(phoneNumber => phoneNumber.telefonszam.indexOf(filter) > -1 ||
        phoneNumber.ugyfel.nev.toLowerCase().indexOf(filter.toLowerCase()) > -1)
      .map((telefonszam, idx) => {
        return (
          <tr key={idx} style={{ cursor: "pointer" }}>
            <td>
              <div onClick={(e) => this.switchEditor(e, telefonszam)}>{telefonszam.telefonszam}</div>
            </td>
            <td>
              <div onClick={(e) => this.switchEditor(e, telefonszam)}>
                {telefonszam.ugyfel ? telefonszam.ugyfel.nev : ""}
              </div>
            </td>
            <td><a style={{ cursor: "pointer" }} onClick={() => { this.deleteTelefonszam(telefonszam); }}>torol</a></td>
          </tr>
        );
      });
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-xs-6" style={{ textAlign: "left" }}>
              <button style={{ marginBottom: "10px", marginRight: "10px" }} className="btn btn-primary"
                onClick={this.addNewTelefonszam.bind(this)}>Uj telefonszam</button>
            </div>
            <div className="col-xs-6" style={{ textAlign: "right" }}>
              <input type="text" className="form-control"
                onChange={(event) => this.filterTelefonszamok(event.target.value)} />
            </div>
          </div>
          <div className="row">
            <div className="col-xs-12">
              <div className="table-responsive">
                <table className="table table-bordered">
                  <thead>
                    <tr>
                      <th>Telefonszam</th>
                      <th>Ugyfel</th>
                      <th>Commands</th>
                    </tr>
                    {this.renderTelefonszamok()}
                  </thead>
                </table>
              </div>
            </div>
          </div>
        </div>
        {this.state.selectedNumber &&
          <div className="modal fade" ref="_modal" id="exampleModalLong" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <TelefonszamEditorModal selectedNumber={this.state.selectedNumber}
              closeEditor={this.closeEditor.bind(this)}
              saveChanges={this.saveChanges.bind(this)}
            />
          </div>
        }
      </div>
    );
  }
}

Telefonszamok.contextTypes = {
  store: PropTypes.object
}

Telefonszamok.propTypes = {
  http: PropTypes.object.isRequired,
}