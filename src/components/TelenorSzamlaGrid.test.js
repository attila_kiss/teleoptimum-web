import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act, Simulate } from "react-dom/test-utils";
import { TelenorSzamlaGrid } from './TelenorSzamlaGrid.js'
import { testData } from './TelenorSzamlaGrid.testdata'

let container = null;
let rows = null;
beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
  rows = JSON.parse(testData);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
  rows = null;
});

it("renders grid with some data", () => {
  act(() => {
    render(<TelenorSzamlaGrid rows={rows} />, container);
  });
  expect(container.getElementsByTagName('tr').length).toBe(201);
  const emptyRow = container.querySelector('tbody tr:nth-child(102)');
  expect(emptyRow.textContent).toBe('');
  expect(container.textContent).toMatch(/Belföldi hívás a Magyar Telekom hálózatába/);
});

it("filter data", () => {
  act(() => {
    render(<TelenorSzamlaGrid rows={rows} />, container);
  });
  const input = container.querySelector('#filter-grid');
  input.value = 'SMS';
  Simulate.change(input);
  expect(container.getElementsByTagName('tr').length).toBe(27);
  expect(container.textContent).toMatch(/SMS hálózaton belül/);
});

it("order data by phonenumber", () => {
  act(() => {
    render(<TelenorSzamlaGrid rows={rows} />, container);
  });

  const header = container.querySelector('thead tr th');
  console.log(header.textContent);

  const phoneCell = container.querySelector('tbody tr td')
  expect(phoneCell.textContent).toBe('20-207-3251');

  act(() => {
    header.dispatchEvent(new MouseEvent('click', {bubbles: true}));
  });

  expect(phoneCell.textContent).toBe('20-215-5844');
  
});

it("order data by description", () => {
  act(() => {
    render(<TelenorSzamlaGrid rows={rows} />, container);
  });

  const header = container.querySelector('thead tr th:nth-child(2)');
  console.log(header.textContent);

  const phoneCell = container.querySelector('tbody tr td:nth-child(2)')
  expect(phoneCell.textContent).toBe('Üzleti elõfizetés');

  act(() => {
    header.dispatchEvent(new MouseEvent('click', {bubbles: true}));
  });

  expect(phoneCell.textContent).toBe('Belföldi hívás a Magyar Telekom hálózatába');
  
});