import React, { Component } from 'react';
import UgyfelEditor from './UgyfelEditor';
import { addCustomer, removeCustomer, filterCustomers, fetchCustomers } from '../store/actions';
import PropTypes from 'prop-types';
import { convertCimToString } from '../utils/Utils';
import Ugyfel from '../model/Ugyfel';

const $ = window.$;

export default class Ugyfelek extends Component {

  constructor(props, context) {
    super(props);
    this.http = props.http;
    this.store = context.store;
    this.state = {};
    this.getUgyfelek();
  }

  getUgyfelek() {
    this.store.dispatch(fetchCustomers(this.http))
  }

  closeModal(reload = false) {
    this.$modal.modal('hide');
    if (!reload && this.state.selectedUgyfel.id === null) {
      this.store.dispatch(removeCustomer(this.state.selectedUgyfel));
    }
    this.setState({ selectedUgyfel: null });
    if (reload) {
      this.getUgyfelek();
    }
  }

  addNewUgyfel() {
    const ugyfel = new Ugyfel();
    this.store.dispatch(addCustomer(ugyfel));
    this.switchEditor(ugyfel);
  }

  switchEditor(ugyfel) {
    this.setState({ selectedUgyfel: ugyfel }, () => {
      this.$modal = $(this.refs._modal);
      this.$modal.modal({ backdrop: 'static', keyboard: false });
    });
  }

  filterUgyfelek(toFilter) {
    this.store.dispatch(filterCustomers(toFilter));
  }

  renderUgyfelRow() {
    const filterText = this.store.getState().customerFilterText;
    console.log(filterText);
    return this.store.getState().customers
      .filter(customer => customer.nev.toLowerCase().indexOf(filterText.toLowerCase()) > -1)
      .map((ugyfel, idx) => {
        return (
          <tr key={idx} className="project">
            <td><span style={{ cursor: "pointer" }} onClick={() => { this.switchEditor(ugyfel) }}>{ugyfel.nev}</span></td>
            <td>
              <ul>
                {ugyfel.telefonszamok.map((telefonszam, idx) => {
                  return <li key={idx}>{telefonszam.telefonszam}</li>
                })}
              </ul>
            </td>
            <td>{ugyfel.email}</td>
            <td>{ugyfel.cimek.map((cim, idx) => { return (<div key={idx}>{cim.tipus}: {convertCimToString(cim)}</div>) })}</td>
          </tr>
        );
    })
  }

  renderUgyfelTabla() {
    return (
      <div>
        <div className="row">
          <div className="col-xs-6" style={{ textAlign: "left" }}>
            <button className="btn btn-primary"
              onClick={this.addNewUgyfel.bind(this)}>Uj ugyfel</button>
          </div>
          <div className="col-xs-6" style={{ textAlign: "right" }}>
            <input type="text" className="form-control"
              onChange={(event) => this.filterUgyfelek(event.target.value)} />
          </div>
        </div>
        <div className="row">
          <div className="col-xs-12">
            <table className="table table-striped" id="ugyfelTable">
              <thead>
                <tr>
                  <th>Nev</th>
                  <th>Telefonszamok</th>
                  <th>Email</th>
                  <th>Cim</th>
                </tr>
              </thead>
              <tbody>
                {this.renderUgyfelRow()}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div>
        <div className="container">
          <div className="row">
            <div className="col-xs-12">
              {this.renderUgyfelTabla()}
              {this.state.selectedUgyfel &&
                <div className="modal fade" ref="_modal" id="exampleModalLong" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                  <UgyfelEditor http={this.props.http}
                    ugyfel={this.state.selectedUgyfel}
                    onClose={this.closeModal.bind(this)} />
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Ugyfelek.contextTypes = {
  store: PropTypes.object
}

Ugyfelek.propTypes = {
  http: PropTypes.object.isRequired
}