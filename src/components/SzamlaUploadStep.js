import React from 'react';

export const SzamlaUploadStep = ({onClick, text, results}) => {

    return (
      <div className="row">
        <div className="col-xs-6">
          <button className="btn btn-primary active" onClick={onClick}>{text}</button>
        </div>
        <div className="col-xs-6">
          {results}
        </div>
      </div>
    );
}