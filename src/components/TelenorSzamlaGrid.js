import React, { Component } from 'react';

export class TelenorSzamlaGrid extends Component {

  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      min: 0,
      max: 100,
      order: { column: 'telefonszam', asc: true}
    };
    this.rendered = [];
    this.scrollTop = -100;
  }


  filterRows() {
    //reset the view port
    this.rendered = [];

    if (!this.state.filterText) {
      return [...this.props.rows];
    } else {
      return this.props.rows.filter(row => {
        return JSON.stringify(row).includes(this.state.filterText);
      });
    }
  }

  checkWhatVisible(event) {
    const scrollTop = event.target.scrollTop;
    let direction = 'DOWN';
    if (this.scrollTop > scrollTop) {
      direction = 'UP';
    }
    this.scrollTop = scrollTop;
    const firstRowInView = Math.floor(scrollTop / 37);
    
    if (direction === 'DOWN') {
      this.setState({ min: (firstRowInView - 20) > 0 ? (firstRowInView - 20) : 0, max: firstRowInView + 80 });
    } else if (direction === 'UP') {
      this.setState({ min: (firstRowInView - 80) > 0 ? (firstRowInView - 80) : 0, max: firstRowInView + 20 });
    }
  }

  orderBy({ column, asc}) {
    return (a, b) => {
      if (typeof a[column] === 'string') {
        return asc ? a[column].localeCompare(b[column]) : b[column].localeCompare(a[column]);
      } else {
        return asc ? a[column] - b[column] : b[column] - a[column];
      }
    };
  }

  setOrdering(column) {
    this.setState({order: {
      asc: column === this.state.order.column ? !this.state.order.asc : this.state.order.asc,
      column: column
    }});
  }

  renderRow(row, index) {
    if (index >= this.state.min && index <= this.state.max) {
      this.rendered.push(index);
      return (
        <tr key={index} ref={index}>
          <td>{row.telefonszam}</td>
          <td>{row.termeknev}</td>
          <td>{row.szamlaTipus}</td>
          <td>{row.tipus}</td>
          <td>{row.tovabbszamlazva}</td>
          <td className="number">{row.mennyiseg.toFixed(2)}</td>
          <td>{row.egyseg}</td>
          <td className="number">{row.nettoegysegar.toFixed(2)}</td>
          <td className="number">{row.afakulcs.toFixed(2)}</td>
          <td className="number">{row.nettoar.toFixed(2)}</td>
          <td className="number">{row.bruttoar.toFixed(2)}</td>
        </tr>);
    } else {
      return (<tr key={index} ref={index}></tr>);
    }
  }

  renderArrow(column) {
    if (column === this.state.order.column && this.state.order.asc) {
      return <span className="glyphicon glyphicon-triangle-bottom"></span>
    } else if (column === this.state.order.column && !this.state.order.asc) {
      return<span className="glyphicon glyphicon-triangle-top"></span>
    }
  }

  render() {
    return (
      <div>
        <div className="row" style={{paddingBottom: "5px"}}>
          <div className="col-xs-6">
          </div>
          <div className="col-xs-6" style={{ textAlign: "right" }}>
            <input type="text" className="form-control" id="filter-grid"
              onChange={(event) => this.setState({filterText: event.target.value})} />
          </div>
        </div>
        <div className="row" style={{ height: '700px', 'overflowY': 'scroll' }} onScroll={e => this.checkWhatVisible(e)}>
          <table className="tableFixHead table table-sm table-hover">
            <thead>
              <tr>
                <th scope="col" onClick={() => this.setOrdering('telefonszam')}>
                  Telefonszám
                  {this.renderArrow('telefonszam')}
                </th>
                <th scope="col" onClick={() => this.setOrdering('termeknev')}>
                  Terméknév
                  {this.renderArrow('termeknev')}
                </th>
                <th scope="col" onClick={() => this.setOrdering('szamlaTipus')}>
                  Szmla Tipus
                  {this.renderArrow('szamlaTipus')}
                </th>
                <th scope="col" onClick={() => this.setOrdering('tipus')}>
                  Tipus
                  {this.renderArrow('tipus')}
                </th>
                <th scope="col">Tovabb</th>
                <th className="number" scope="col">Mennyiség</th>
                <th scope="col">Egység</th>
                <th className="number" scope="col">Egység Ár</th>
                <th className="number" scope="col">Áfakulcs</th>
                <th className="number" scope="col">Nettó Ár</th>
                <th className="number" scope="col">Brutto Ár</th>
              </tr>
            </thead>
            <tbody>
              {this.filterRows().sort(this.orderBy(this.state.order))
                .map((row, index) => {
                  return this.renderRow(row, index);
                })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
