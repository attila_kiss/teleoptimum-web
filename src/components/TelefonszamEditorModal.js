import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TelefonszamEditorModal extends Component {

  constructor(props, context) {
    super(props, context);
    this.store = context.store;
  }

  updateUgyfel(value, telefonszam) {
    telefonszam.ugyfelId = parseInt(value);
    telefonszam.ugyfel = this.store.getState().customers.find(ugyfel => ugyfel.id === parseInt(value));
  }

  updateTelefonszam(value, telefonszam) {
    telefonszam.telefonszam = value;
  }

  render() {
    return (
      <div className="modal-dialog" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLongTitle">Modal title</h5>
            <button type="button" className="close" onClick={this.props.closeEditor} aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <form>
              <div className="form-group">
                <label htmlFor="telefonszam">Telefonszam</label>
                <input type="text" name="telefonszam" className="form-control" defaultValue={this.props.selectedNumber.telefonszam}
                      onChange={(event) => this.updateTelefonszam(event.target.value, this.props.selectedNumber)}/>
              </div>
              <div className="form-group">
                <label htmlFor="ugyfel">Ugyfel</label>
                <select name="ugyfel" defaultValue={this.props.selectedNumber.ugyfelId} className="form-control" style={{width: "inherit"}}
                    onChange={(event) => this.updateUgyfel(event.target.value, this.props.selectedNumber)}>
                  {this.store.getState().customers.map(ugyfel => {
                    return <option key={ugyfel.id} value={ugyfel.id}>{ugyfel.nev}</option>
                  })}
                </select>
              </div>
            </form>
          </div>
          <div className="modal-footer">
            <button type="button" className="btn btn-secondary" onClick={this.props.closeEditor}>Close</button>
            <button type="button" className="btn btn-primary" onClick={() => this.props.saveChanges(this.props.selectedNumber)}>Save changes</button>
          </div>
        </div>
      </div>
    );
  }
}

TelefonszamEditorModal.propTypes = {
  selectedNumber: PropTypes.object.isRequired,
  ugyfelek: PropTypes.array.isRequired,
  closeEditor: PropTypes.func.isRequired,
  saveChanges: PropTypes.func.isRequired
}

TelefonszamEditorModal.defaultProps = {
  selectedNumber: { telefonszam: {} },
  ugyfelek: [],
  closeEditor: () => {},
  saveChanges: () => {}
}

TelefonszamEditorModal.contextTypes = {
  store: PropTypes.object
}