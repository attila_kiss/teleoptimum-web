import React, { Component } from 'react';
import { Enums, TarifaTipusSelector } from '../utils/TarifaTipusSelector.js'
import { TelenorSzamlaGrid } from './TelenorSzamlaGrid.js'
import JsonUtils from '../utils/JsonUtils.js';
import { korlatlanTarifa, simaTarifa } from '../model/Tarifak';
import { parseString } from 'xml2js';
import PropTypes from 'prop-types'
import { fetchCustomers, addInvoicePhoneNumbers, addPeriod, addInvoiceNumber, populateInvoiceRows } from '../store/actions'
import { SzamlaUploadStep } from './SzamlaUploadStep.js';

export default class SzamlaUpload extends Component {

  constructor(props, context) {
    super(props, context);
    this.steps = {
      CHECK_TELEFONSZAM: {
        text: 'Telefonszamok ellenorzese',
        onClick: this.checkTelefonszamok.bind(this),
      },
      MERGE: {
        text: 'Azonos tetel osszevonasa',
        onClick: this.mergeRows.bind(this),
      },
      DISCOUNTS: {
        text: 'Kedvezmenyek szamitasa',
        onClick: this.findTipusKedvezmeny.bind(this),
      },
      CONVERT: {
        text: 'Tetelek konvertalasa',
        onClick: this.convertTetelek.bind(this),
      },
      GENERATE: {
        text: 'Szamlak letrehozasa',
        onClick: this.navigateToSzamlaGenerator.bind(this),
      }
    }
    this.http = props.http;
    this.store = context.store;
    this.state = {
      working: false,
      idoszak: '',
      sorszam: '',
      filters: {},
      brutto: 0,
      results: '',
      step: this.steps.CHECK_TELEFONSZAM
    }
    this.store.dispatch(fetchCustomers(this.http));
  }

  handleFileUpoad(event) {
    let f = event.target.files[0]
    let reader = new FileReader();
    this.setState({ working: true }, () => {
      reader.onloadend = () => {
        parseString(reader.result, (err, szamlaXml) => {
          this.store.dispatch(addPeriod(szamlaXml.foszamla.szamla[0].fejlec[0].szamlainfo[0].idoszak[0]));
          this.store.dispatch(addInvoiceNumber(szamlaXml.foszamla.szamla[0].fejlec[0].szamlainfo[0].sorszam[0]));
          let rows = [];
          let telefonszamok = this.store.getState().invoicePhoneNumbers || {};
          let brutto = this.state.brutto;
          let mobilSzamok = szamlaXml.foszamla.szamla.reduce((szamok, szamla) => {
            return szamok.concat(szamla.tetelek[0].kartyaszintutetelek[0].mobilszam);
          }, []);
          mobilSzamok.forEach(item => {
            if (item == null) {
              return;
            }
            let telefonszam = item.$.ctn;
            if (!telefonszamok[telefonszam]) {
              telefonszamok[telefonszam] = { telefonszam: telefonszam, tipus: 'sima', kedvezmeny: 0, ugyfel: {}, kartyatipus: 'adat' };
            }
            const processJson = (tetel, tipus) => {
              const row = this.mapTetel(tetel, tipus);
              row.id = rows.length + 1;
              row.telefonszam = telefonszam;
              rows.push(row);
              if (row.nettoegysegar === 5000 && row.termeknev === 'Üzleti elõfizetés') {
                telefonszamok[telefonszam].tipus = 'korlatlan';
              }
              if (row.termeknev === 'Üzleti elõfizetés') {
                telefonszamok[telefonszam].kartyatipus = 'hangalapu';
              }
              brutto += row.bruttoar;
            }

            JsonUtils.traverseJson(item, processJson, '');
          });
          this.store.dispatch(addInvoicePhoneNumbers(telefonszamok));
          this.store.dispatch(populateInvoiceRows(rows));
          this.setState({ brutto: brutto, working: false });
        });
      }
    });
    reader.readAsText(f);
  }

  parseMennyiseg(mennyiseg) {
    if (!mennyiseg) {
      return 1;
    }
    mennyiseg = mennyiseg[0];
    if (mennyiseg.match(/(.+):(.+):(.+)/)) {
      let timeInMinutes = 0;
      let idok = mennyiseg.split(":");
      timeInMinutes += parseInt(idok[0]) * 60;
      timeInMinutes += parseInt(idok[1]);
      timeInMinutes += parseInt(idok[2]) / 60;
      return +(timeInMinutes.toFixed(2));
    } else {
      return parseFloat(mennyiseg.replace(',', '.'));
    }
  }

  mapTetel(tetel, tipus) {
    let row = {};
    row.termeknev = tetel.termeknev[0];
    row.mennyiseg = this.parseMennyiseg(tetel.menny);
    tetel.mennyegys ? row.egyseg = tetel.mennyegys[0] : row.egyseg = '';
    row.bruttoar = parseFloat(tetel.bruttoar[0].replace(',', '.'));
    row.nettoar = tetel.nettoar ? parseFloat(tetel.nettoar[0].replace(',', '.')) : row.bruttoar;
    row.tipus = (tipus === '' ? TarifaTipusSelector.findTipus(row.termeknev) : tipus);
    tetel.afakulcs && tetel.afakulcs[0] ? row.afakulcs = parseInt(tetel.afakulcs[0]) : row.afakulcs = (function () {
      return Math.round((row.bruttoar / row.nettoar - 1) * 100);
    })();
    let egysegAr = tetel.nettoegysegar ? tetel.nettoegysegar[0] : "";
    egysegAr.match(/\d+/) ? row.nettoegysegar = parseFloat(egysegAr.replace(',', '.')) : row.nettoegysegar = (function () {
      return row.nettoar / row.mennyiseg;
    })();
    row.szamlaTipus = 'ismeretlen';
    row.tovabbszamlazva = true;
    return row;
  }

  editEuRoaming(row) {
    if (row.termeknev.toLowerCase().includes("roaming") && row.nettoegysegar === 6) {
      row.egysegAr = 9.0;
      row.nettoegysegar = 9.0;
      row.nettoar = row.egysegAr * row.mennyiseg;
      row.bruttoar = row.nettoar * (100 + row.afakulcs) / 100;
    }
    return row;
  }

  checkTelefonszamok() {
    let missingPhoneNumber = false;
    let results = 'hianyzo telefonszamok:'
    const origTelefonszamok = this.store.getState().invoicePhoneNumbers;
    let telefonszamok = {};
    Object.keys(origTelefonszamok).forEach((key) => {
      const telefonszamObj = origTelefonszamok[key];
      const telefonszam = key.replace(new RegExp('-', 'g'), '');
      const ugyfel = this.store.getState().customers.find(elem => {
        const szam = elem.telefonszamok.find(teloszam => teloszam.telefonszam === telefonszam)
        return szam && szam.telefonszam === telefonszam;
      });
      if (ugyfel) {
        telefonszamObj.ugyfel = ugyfel;
        telefonszamok[key] = telefonszamObj
      } else {
        results += ` ${telefonszam}`;
        this.setState({ results });
        missingPhoneNumber = true;
      }
    });
    if (!missingPhoneNumber) {
      this.store.dispatch(addInvoicePhoneNumbers(telefonszamok));
      this.setState({ step: this.steps.MERGE, telefonszamok })
    }
  }

  findTipusKedvezmeny() {
    const telefonszamok = this.store.getState().invoicePhoneNumbers;
    const rows = this.store.getState().invoiceRows.map(row => {
      const additionalInfo = telefonszamok[row.telefonszam];
      const oneToOne = additionalInfo.ugyfel.needsSpecialInvoice === 3
      row.szamlaTipus = additionalInfo.tipus;
      row.tovabbszamlazva = oneToOne || Enums[row.tipus].tovabbszamlazva[row.szamlaTipus];
      if (!oneToOne) {
        if (row.szamlaTipus === 'sima' && TarifaTipusSelector.isKedvezmenyes(row.tipus)) {
          additionalInfo.kedvezmeny += row.mennyiseg;
        }
      }
      return row;
    });
    this.store.dispatch(populateInvoiceRows(rows));
    this.setState({ step: this.steps.CONVERT });
  }

  mergeRows() {
    const telefonszamok = this.store.getState().invoicePhoneNumbers;
    try {
      const rows = this.store.getState().invoiceRows.reduce((rows, row) => {
        if (telefonszamok[row.telefonszam].ugyfel.needsSpecialInvoice === 3) {
          return [...rows, row];
        } else if (telefonszamok[row.telefonszam].ugyfel.needsSpecialInvoice === 1) {
          return rows;
        } else {
          let merged = false;
          for (let i = 0; i < rows.length; i++) {
            let currentRow = rows[i];
            if (currentRow.telefonszam === row.telefonszam && currentRow.termeknev === row.termeknev) {
              currentRow.mennyiseg += row.mennyiseg;
              currentRow.nettoar += row.nettoar;
              currentRow.bruttoar += row.bruttoar;
              merged = true;
              break;
            }
          }
          return merged ? rows : [...rows, row];
        }
      }, []);
      this.store.dispatch(populateInvoiceRows(rows));
      this.setState({ step: this.steps.DISCOUNTS });
    } catch (e) {
      console.log(e);
      this.setState({ results: e.message });
    }
  }

  convertTetelek() {
    let brutto = 0;
    let szorzo = 1;
    let idx = 0;
    const telefonszamok = this.store.getState().invoicePhoneNumbers;
    const rows = this.store.getState().invoiceRows
      .map((row, index) => {
        idx = index;
        if (telefonszamok[row.telefonszam].ugyfel.needsSpecialInvoice === 3) {
          return row;
        } else if (telefonszamok[row.telefonszam].ugyfel.needsSpecialInvoice === 1) {
          return null;
        } else {
          row = this.editEuRoaming(row);
          let tarifa = []
          if (row.szamlaTipus === 'sima') {
            tarifa = simaTarifa;
          } else {
            tarifa = korlatlanTarifa;
          }
          let tarifaElem = tarifa.find(element => element.tipus === row.tipus);
          if (Enums[row.tipus].egyseg) {
            row.egyseg = Enums[row.tipus].egyseg;
            row.mennyiseg = Enums[row.tipus].mennyiseg;
          }
          if (tarifaElem) {
            row.nettoegysegar = tarifaElem.nettoegysegar;
            row.afakulcs = tarifaElem.afakulcs;
            row.nettoar = row.nettoegysegar * row.mennyiseg;
            row.bruttoar = row.nettoar * (100 + row.afakulcs) / 100;
          }
          szorzo = this.adjustHavidijak(row);
          if (row.tovabbszamlazva) {
            brutto += row.bruttoar;
          }
          return row;
        }
      })
      .concat(Object.keys(telefonszamok).map(key => {
        const telProp = telefonszamok[key];
        if (telProp.ugyfel.needsSpecialInvoice === 1 || telProp.ugyfel.needsSpecialInvoice === 3) {
          return null;
        }
        if (telProp.kedvezmeny > 0) {
          const kedv = this.createForgalmidijKedvezmenyTetel(++idx, telProp);
          brutto += kedv.bruttoar;
          return kedv;
        }
        if (telProp.tipus === 'sima' && telProp.kartyatipus === "hangalapu") {
          const egycsoport = this.createEgycsoport(++idx, telProp, szorzo);
          brutto += egycsoport.bruttoar;
          return egycsoport;
        }
        return null;
      }))
      .filter(row => row !== null);
      this.store.dispatch(populateInvoiceRows(rows));
    this.setState({ brutto, step: this.steps.GENERATE });
  }

  adjustHavidijak(row) {
    let szorzo = 1;
    if (row.tipus.match(/(HAVIDIJ|MOBIL_INTERNET_*)/) && row.nettoar / row.nettoegysegar < 1) {
      szorzo = row.nettoar / row.nettoegysegar;
      row.nettoar = row.nettoegysegar * szorzo;
      row.bruttoar = row.nettoar * (100 + row.afakulcs) / 100;
    }
    return szorzo;
  }

  createForgalmidijKedvezmenyTetel(id, telefonszamProp) {
    let tetel = {};
    tetel.id = id;
    tetel.telefonszam = telefonszamProp.telefonszam;
    tetel.termeknev = 'Forgalmidij-kedvezmeny';
    tetel.mennyiseg = Math.min(telefonszamProp.kedvezmeny, 150);
    tetel.egyseg = 'perc';
    tetel.nettoegysegar = -9;
    tetel.nettoar = tetel.nettoegysegar * tetel.mennyiseg;
    tetel.afakulcs = 27;
    tetel.bruttoar = tetel.nettoar * (100 + tetel.afakulcs) / 100;
    tetel.tipus = 'KEDVEZMENY';
    tetel.szamlaTipus = 'sima';
    tetel.tovabbszamlazva = true;
    return tetel;
  }

  createEgycsoport(id, telefonszamProp, szorzo) {
    let tetel = {};
    tetel.id = id;
    tetel.telefonszam = telefonszamProp.telefonszam;
    tetel.termeknev = 'Egycsoport';
    tetel.mennyiseg = 1;
    tetel.egyseg = 'honap';
    tetel.nettoegysegar = 400;
    tetel.nettoar = tetel.nettoegysegar * tetel.mennyiseg * szorzo;
    tetel.afakulcs = 27;
    tetel.bruttoar = tetel.nettoar * (100 + tetel.afakulcs) / 100;
    tetel.tipus = 'EGYEB';
    tetel.szamlaTipus = 'sima';
    tetel.tovabbszamlazva = true;
    return tetel;
  }

  updateRows(rows, filters) {
    this.setState({ rows: rows, filters: filters })
  }

  navigateToSzamlaGenerator() {
    localStorage.setItem('telefonszamok', JSON.stringify(this.state.telefonszamok));
    localStorage.setItem('rows', JSON.stringify(this.state.rows));
    localStorage.setItem('idoszak', this.state.idoszak);
    localStorage.setItem('sorszam', this.state.sorszam);
    window.location.replace('#/generate');
  }

  render() {
    const { period, invoiceNumber } = this.store.getState();
    return (
      <div className="container" style={{ width: '80%' }}>
        <div className="row">
          <div className="col-xs-6">
            <form>
              <div className="form-group">
                <label htmlFor="szamlaInput">Havi szamla</label>
                <input type="file" id="szamlaInput" onChange={this.handleFileUpoad.bind(this)} />
              </div>
            </form>
          </div>
          <div className="col-xs-6" style={{ float: "right" }}>
            {this.state.working && <div>Working...</div>}
            {!this.state.working &&
              <div>
                <div className="help-block" style={{ float: "right", paddingLeft: "10px" }}>Idoszak: {period}</div>
                <div className="help-block" style={{ float: "right", paddingLeft: "10px" }}>Szamla sorszam: {invoiceNumber}</div>
                <div className="help-block" style={{ float: "right", paddingLeft: "10px" }}>Brutto: {this.state.brutto.toFixed(2)}</div>
              </div>
            }
          </div>
        </div>
        <TelenorSzamlaGrid rows={this.store.getState().invoiceRows} updateRows={this.updateRows.bind(this)} filters={this.state.filters} />
        <SzamlaUploadStep {...this.state.step} results={this.state.results} />
      </div>
    );
  }
}

SzamlaUpload.propTypes = {
  http: PropTypes.object.isRequired,
}

SzamlaUpload.contextTypes = {
  store: PropTypes.object
}
