export default class Ugyfel {
  id = null;
  version = 0;
  nev = "";
  ugyfelKod = "";
  fizetesiMod = "";
  szamlaSzam =  "";
  email =  "";
  adoszam =  "";
  szamlaEmailben =  false;
  ugyfelTipus = "magan";
  ceg_jegyzek_szam =  "";
  kacsolat_tarto_nev =  "";
  kapcsolat_tarto_sz_ig_szam =  "";
  kapcsolat_tarto_tsz =  "";
  anyja_neve =  "";
  egyeb_telefonszam =  "";
  sz_hely_ido =  "";
  szem_ig_szam =  "";
  needsSpecialInvoice =  0;
  cimek = [
    {
      orszag: "Magyarország",
      varos: "",
      kozterulet: "",
      kozterulet_tipus: "",
      haz_szam: "",
      emelet: "",
      ajto: "",
      iranyitoszam: '',
      tipus: "ALLANDO"
    },
    {
      orszag: "Magyarország",
      varos: "",
      kozterulet: "",
      kozterulet_tipus: "",
      haz_szam: "",
      emelet: "",
      ajto: "",
      iranyitoszam: '',
      tipus: "LEVELEZESI"
    }
  ];
  telefonszamok = []
}