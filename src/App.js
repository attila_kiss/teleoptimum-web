import React, { Component } from 'react';
import Ugyfelek from './components/Ugyfelek';
import Telefonszamok from './components/Telefonszamok';
import TeleoptimumMenu from './components/TeleoptimumMenu';
import SzamlazoExport from './components/SzamlazoExport'
import SzamlaUpload from './components/SzamlaUpload'
import Login from './components/Login';
import './App.css';
import NetworkUtils from './utils/Networkutils';
import PropTypes from 'prop-types';

class App extends Component {

  constructor(props) {
    super(props);
    this.user = JSON.parse(localStorage.getItem('user'));
    if (this.user) {
      this.http = new NetworkUtils(this.user.username, this.user.password)
      this.state = {
        component: <Telefonszamok http={this.http}/>,
        location: 'telefonszamok'
      };
    }
    window.location.hash = '#telefonszamok';
    window.addEventListener('hashchange', this.handleNewHash.bind(this), false);
  }

  getChildContext() {
    return {
      store: this.props.store
    }
  }

  componentWillMount() {
    this.unsubscribe = this.props.store.subscribe(
      () => this.forceUpdate()
    )
  }

  componentWillUnmount() {
    this.unsubscribe()
  }

  setUser(username, password) {    
    this.user = {
      username: username,
      password: password
    }
    localStorage.setItem('user', JSON.stringify(this.user));
  }

  handleNewHash() {
    const location = window.location.hash.replace(/^#\/?|\/$/g, '').split('/');
    console.log(location);
    this.setState({ component: this.getComponent(location), location: location[0] });
  }

  getComponent(location) {
    switch (location[0])  {
      case '':
      case 'telefonszamok':
        return <Telefonszamok http={this.http} />
      case 'ugyfelek':
        return <Ugyfelek http={this.http} />
      case 'export':
        return <SzamlazoExport http={this.http}/>
      case 'upload':
        return <SzamlaUpload http={this.http}/>;
      default:
        return <div><h1>Not Found</h1></div>;
      }
  }

  render() {
    if (!this.user){
      return <Login setUser={this.setUser.bind(this)} />;
    }
    return (
      <div className="App">
        <TeleoptimumMenu active={this.state.location}/>
        { this.state.component }
      </div>
    );
  }
}

App.propTypes = {
  store: PropTypes.object.isRequired
}

App.childContextTypes = {
  store: PropTypes.object.isRequired
}

export default App;

