export function convertNullToEmpty(object) {
  return object ? object : {}
}

export function isBlank(param ){
  return (param === null || param.trim() === "");
}

export function isNotBlank(param) {
  return !isBlank(param);
}

export function convertCimToString(cim) {
  var strCim = "";
  strCim += cim.kozterulet + " ";
  strCim += isNotBlank(cim.kozterulet_tipus) ? cim.kozterulet_tipus + " " : "";
  strCim += isNotBlank(cim.haz_szam) ? cim.haz_szam + " " : "";
  strCim += isNotBlank(cim.emelet) ? cim.emelet + " em " : "";
  strCim += isNotBlank(cim.ajto) ? cim.ajto : "";
  strCim += " " + cim.varos + " " + cim.iranyitoszam;
  return strCim;
}

export function isInViewport(elem) {
  var bounding = elem.getBoundingClientRect();
  return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};