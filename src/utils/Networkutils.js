export default class NetworkUtils {

  constructor(username, password) {
    this.headers = new Headers({
      'Content-Type': 'application/json', 
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Request-Headers': 'X-Requested-With',
      'Authorization': 'Basic '+btoa(username + ":" + password)
    });
  }

  fetch(method, url, body) {
    let request= {
      method: method,
      headers: this.headers
    };
    request.body = this.prepareBody(body);
    return fetch(window.CONFIG.backend + url, request);
  }

  prepareBody(body) {
    if(typeof body === "string" || body instanceof String) {
      return body;
    } else {
      return JSON.stringify(body);
    }
  }

  prepareParams(szamlak){
    if (Array.isArray(szamlak)){
      return JSON.stringify(szamlak);
    }else{
      return JSON.stringify([szamlak])
    }
  }

  postSzamlak(szamlak){
    return fetch(window.CONFIG.backend + '/szamlak', {
      method: 'POST',
      headers: this.headers,
      body: this.prepareParams(szamlak)
    })
    .then((response) => {
      if (response.ok){
        return response.json();
      }else{
        throw Error("Szamla generalas nem sikerult: " + response.text());
      }
    });
  }

	generateSzamlaKep(szamlak){
		return fetch(window.CONFIG.backend + '/szamlak/generate', {
			method: 'POST',
			headers: this.headers,
			body: this.prepareParams(szamlak)
		})
		.then((response) => {
			if (response.ok){
				return response.json();
			}else{
				throw Error("Szamla generalas nem sikerult: " + response.text());
			}
		});
	}

}