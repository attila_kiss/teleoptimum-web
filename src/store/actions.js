import C from './constants'

export const addPhoneNumber = ({id, telefonszam, ugyfelId, ugyfel}) =>
({
  type: C.ADD_PHONENUMBER,
  id,
  telefonszam,
  ugyfelId,
  ugyfel
});

export const editPhoneNumber = ({id, telefonszam, ugyfelId, ugyfel}) =>
({
  type: C.EDIT_PHONENUMBER,
  id,
  telefonszam,
  ugyfelId,
  ugyfel
});

export const removePhoneNumber = phoneNumber =>
({
  type: C.REMOVE_PHONENUMBER,
  phoneNumber
});

export const fetchPhoneNumbers = (http) => {
  return (dispatch) => {
    return http.fetch('GET', '/telefonszamok').then(response => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response.json();
    })
    .then(phoneNumbers => {
      dispatch(populatePhoneNumbers(phoneNumbers));
      return phoneNumbers;
    })
  };
};

export const populatePhoneNumbers = (phoneNumbers) =>
({
  type: C.POPULATE_PHONENUMBERS,
  phoneNumbers
});

export const filterPhoneNumbers = (searchString) => ({
  type: C.FILTER_PHONENUMBERS,
  searchString
});

export const addCustomer = (customer) =>
({
  type: C.ADD_CUSTOMER,
  ...customer
});

export const removeCustomer = (customer) =>
({
  type: C.REMOVE_CUSTOMER,
  ...customer
});

export const editCustomer = customer => ({
  type: C.EDIT_CUSTOMER,
  ...customer
})

export const populateCustomers = (customers) =>
({
  type: C.POPULATE_CUSTOMERS,
  customers
});

export const fetchCustomers = (http) => {
  return (dispatch) => {
    return http.fetch('GET', '/ugyfelek').then(response => {
      if (!response.ok) {
        throw Error(response.statusText)
      }
      return response.json();
    })
    .then(customers => {
      dispatch(populateCustomers(customers));
      return customers;
    });
  }
}

export const filterCustomers = (searchString) => ({
  type: C.FILTER_CUSTOMERS,
  searchString
});

export const addInvoicePhoneNumbers = invoicePhoneNumbers => ({
  type: C.ADD_INVOICE_PHONENUMBERS,
  invoicePhoneNumbers
});

export const addInvoiceNumber = invoiceNumber => ({
  type: C.ADD_INVOICE_NUMBER,
  invoiceNumber
});

export const addPeriod = period => ({
  type: C.ADD_PERIOD,
  period
});

export const addInvoceRow = ({id, telefonszam, termeknev, mennyiseg, bruttoar, nettoar, tipus, szamlaTipus, tovabbszamlazva}) =>
({
  type: C.ADD_INVOICE_ROW,
  id,
  telefonszam,
  termeknev, 
  mennyiseg, 
  bruttoar, 
  nettoar, 
  tipus, 
  szamlaTipus, 
  tovabbszamlazva
});

export const populateInvoiceRows = rows => ({
  type: C.POPULATE_INVOICE_ROWS,
  rows
})