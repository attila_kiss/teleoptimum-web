import { createStore, combineReducers, applyMiddleware } from 'redux'
import { phoneNumbers, phoneFilterText, customers, customerFilterText, invoicePhoneNumbers, invoiceNumber, period, invoiceRows } from './reducers'
import C from './constants'
import thunk from 'redux-thunk'

const changes = [];
const collectPhoneNumberChanges = store => next => action => {
  let result = next(action);
  if ([C.ADD_PHONENUMBER, C.EDIT_PHONENUMBER, C.REMOVE_PHONENUMBER].includes(action.type)) {
    changes.push(action);
  }
  return result;
}

const storeFactory = (initialState = {}) =>
  applyMiddleware(thunk)(createStore)(
    combineReducers({ phoneNumbers, customers, phoneFilterText, customerFilterText, invoicePhoneNumbers, invoiceNumber, period, invoiceRows }),
    (localStorage['redux-store']) ?
      JSON.parse(localStorage['redux-store']) :
      initialState
  )

export {storeFactory, changes}