const constants = {
    ADD_PHONENUMBER: "ADD_PHONENUMBER",
    EDIT_PHONENUMBER: "EDIT_PHONENUMBER",
    REMOVE_PHONENUMBER: "REMOVE_PHONENUMBER",
    POPULATE_PHONENUMBERS: "POPULATE_PHONENUMBERS",
    FILTER_PHONENUMBERS: "FILTER_PHONENUMBERS",
    ADD_CUSTOMER: "ADD_CUSTOMER",
    REMOVE_CUSTOMER: "REMOVE_CUSTOMER",
    EDIT_CUSTOMER: "EDIT_CUSTOMER",
    POPULATE_CUSTOMERS: "POPULATE_CUSTOMERS",
    FILTER_CUSTOMERS: "FILTER_CUSTOMERS",
    ADD_INVOICE_PHONENUMBERS: "ADD_INVOICE_PHONENUMBERS",
    ADD_PERIOD: "ADD_PERIOD",
    ADD_INVOICE_NUMBER: "ADD_INVOICE_NUMBER",
    ADD_INVOICE_ROW: "ADD_INVOICE_ROW",
    POPULATE_INVOICE_ROWS: "POPULATE_INVOICE_ROWS"

}

export default constants;