import C from './constants'

export const phoneNumber = (state = {}, action) => {
  switch (action.type) {
    case C.ADD_PHONENUMBER:
      return {
        id: action.id,
        telefonszam: action.telefonszam,
        ugyfelId: action.ugyfelId,
        ugyfel: action.ugyfel
      };
    case C.EDIT_PHONENUMBER:
      if (state.id === action.id) {
        return {
          id: state.id,
          telefonszam: action.telefonszam,
          ugyfelId: action.ugyfelId,
          ugyfel: action.ugyfel
        };
      } else {
        return state;
      }
    default:
      return state;
  }
}

export const phoneNumbers = (state = [], action) => {
  switch (action.type) {
    case C.ADD_PHONENUMBER:
      return [
        phoneNumber({}, action),
        ...state
      ];
    case C.EDIT_PHONENUMBER:
      return state.map(
        c => phoneNumber(c, action)
      );
    case C.REMOVE_PHONENUMBER:
      return state.filter(phoneNumber => phoneNumber.id !== action.phoneNumber.id);
    case C.POPULATE_PHONENUMBERS:
      return action.phoneNumbers;
    default:
      return state;
  }
}

export const phoneFilterText = (state = '', action) => {
  switch (action.type) {
    case C.FILTER_PHONENUMBERS:
      return action.searchString
    default:
      return state; 
  }
}

export const customer = (state = {}, action) => {
  const {type, ...customer} = action;
  switch (type) {
    case C.ADD_CUSTOMER:
      return {
        ...action
      };
    case C.EDIT_CUSTOMER:
      if (state.id === customer.id) {
        return customer;
      } else {
        return state;
      }
    default:
      return state;
  }
}

export const customers = (state = [], action) => {
  switch (action.type) {
    case C.ADD_CUSTOMER:
      return [
        customer({}, action),
        ...state
      ];
    case C.REMOVE_CUSTOMER:
      return state.filter(customer => customer.id !== action.id);
    case C.EDIT_CUSTOMER:
      return state.map(
        c => customer(c, action)
      );
    case C.POPULATE_CUSTOMERS:
      return action.customers;
    default:
      return state;
  }
}

export const customerFilterText = (state = '', action) => {
  switch (action.type) {
    case C.FILTER_CUSTOMERS:
      return action.searchString;
    default:
      return state; 
  }
}

export const invoicePhoneNumbers = (state = {}, action) => {
  switch (action.type) {
    case C.ADD_INVOICE_PHONENUMBERS:
      return action.invoicePhoneNumbers;
    default:
      return state; 
  }
}

export const invoiceNumber = (state = '', action) => {
  switch (action.type) {
    case C.ADD_INVOICE_NUMBER:
      return action.invoiceNumber;
    default:
      return state; 
  }
}

export const period = (state = '', action) => {
  switch (action.type) {
    case C.ADD_PERIOD:
      return action.period;
    default:
      return state; 
  }
}

export const invoiceRows = (state = [], action) => {
  switch (action.type) {
    case C.POPULATE_INVOICE_ROWS:
      return action.rows;
    default:
      return state;
  }
}